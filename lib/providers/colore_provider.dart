import 'package:flutter/material.dart';

class ColoresProvider with ChangeNotifier {
  String _color = 'ROJO';

  get color {
    return _color;
  }

  set color(String color) {
    _color = color;
    notifyListeners();
  }
}
