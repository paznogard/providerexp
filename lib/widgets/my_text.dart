import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerex/providers/colore_provider.dart';

class MyText extends StatelessWidget {
  int contador = 1;
  @override
  Widget build(BuildContext context) {
    contador++;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Consumer<ColoresProvider>(builder: (context, coloresProvider, _) {
          return Text(coloresProvider.color, style: TextStyle(fontSize: 30));
        }),
        Text(
          'Count:' + counter().toString(),
          style: TextStyle(fontSize: 30),
        )
      ],
    );
  }

  int counter() {
    return contador++;
  }
}
