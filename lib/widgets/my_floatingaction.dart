import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerex/providers/colore_provider.dart';

class MyFloatingAction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ColoresProvider coloresProvider = Provider.of<ColoresProvider>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        FloatingActionButton(
            child: Icon(Icons.ac_unit_outlined),
            backgroundColor: Colors.red,
            onPressed: () => coloresProvider.color = 'rojo'),
        SizedBox(
          height: 15.0,
        ),
        FloatingActionButton(
            child: Icon(Icons.access_alarm),
            backgroundColor: Colors.blue,
            onPressed: () => coloresProvider.color = 'azul'),
      ],
    );
  }
}
