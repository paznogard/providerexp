import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerex/pages/home_page.dart';
import 'package:providerex/providers/colore_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ColoresProvider()),
      ],
      child: MaterialApp(
        title: 'Material App',
        initialRoute: 'home',
        routes: {'home': (context) => HomaePage()},
      ),
    );
  }
}
