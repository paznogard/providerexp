import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerex/providers/colore_provider.dart';
import 'package:providerex/widgets/my_floatingaction.dart';
import 'package:providerex/widgets/my_text.dart';

class HomaePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final ColoresProvider coloresProvider = Provider.of<ColoresProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Mi Provider'),
      ),
      body: Center(child: MyText()),
      floatingActionButton: MyFloatingAction(),
    );
  }
}
